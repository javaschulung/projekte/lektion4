package de.ruv.javaschulung.Lektion4Vererbung;

public class Datum {

	private int tag;
	private int monat;
	private int jahr;
	
	
	public Datum(int tag, int monat, int jahr) {
		this.setJahr(jahr);
		this.setMonat(monat);
		this.setTag(tag);
	}
	
	public Datum(int monat, int jahr) {
		this(1, monat, jahr);
	}
	
	public Datum(int jahr) {
		this(1, 1, jahr);
	}
	
	public int getTag() {
		return this.tag;
	}
	
	public int getMonat() {
		return this.monat;
	}
	
	public int getJahr() {
		return this.jahr;
	}
	
	private void setTag(int tag) {
		if (tag <= 0 || tag > this.anzahlTageImMonat()) {
			throw new IllegalArgumentException("Tag ist falsch");
		}
		this.tag = tag;
	}
	
	private void setMonat(int monat) {
		this.monat = monat;
	}
	
	private void setJahr(int jahr) {
		this.jahr = jahr;
	}
	
	public boolean istSchaltjahr() {
		boolean durch4 = this.jahr % 4 == 0;
		boolean durch100 = this.jahr % 100 == 0;
		boolean durch400 = this.jahr % 400 == 0;
		
		return durch4 && (!durch100 || durch400);
	}
	
	public int anzahlTageImMonat() {
		switch (this.monat) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: return 31;
		
		case 4:
		case 6:
		case 9:
		case 11: return 30;
		
		case 2: return (this.istSchaltjahr())? 29 : 28;
		
		default: throw new RuntimeException();
		}
	}
	
	public boolean istKleinerAls(Datum that) {		
		if (this.jahr < that.jahr)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat < that.monat)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat == that.monat
				&& this.tag < that.tag)
			return true;
		
		return false;
	}
	
	public boolean istGroesserAls(Datum that) {
		if (this.jahr > that.jahr)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat > that.monat)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat == that.monat
				&& this.tag > that.tag)
			return true;
		
		return false;
	}
	
	public boolean istGleicherTagAls(Datum that) {
		if (that == null)
			return false;
		
		if (this == that) 
			return true;
		
		if (this.jahr != that.jahr)
			return false;
		
		if (this.monat != that.monat)
			return false;
		
		if (this.tag != that.tag)
			return false;
		
		return true;
	}
	
	//==================================
	// toString am besten hier
	//==================================
	
	
}
