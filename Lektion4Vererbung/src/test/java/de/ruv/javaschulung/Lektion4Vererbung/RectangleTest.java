package de.ruv.javaschulung.Lektion4Vererbung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RectangleTest extends TestCase {

	public RectangleTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(RectangleTest.class);
	}

	public void testRectangleKonstruktion() {
		
		Vector v1 = new Vector(-1.0, 1.0);
		Vector v2 = new Vector(2.0, -1.5);
		
		Rectangle rec1 = new Rectangle(v1, 3.14, 2.8, v2);
		
		assertEquals(new Vector(-1.0, 1.0), rec1.getPosition());
		assertSame(v1, rec1.getPosition());
		assertEquals(new Vector(2.0, -1.5), rec1.getMovement());
		assertSame(v2, rec1.getMovement());
		
		assertEquals(3.14, rec1.getWidth());
		assertEquals(2.8, rec1.getHeight());
		
		Vector v3 = new Vector(21.0, -42.42);
		Vector v4 = new Vector(-127.128, 0.0);
		
		Rectangle rec2 = new Rectangle(v3, 10.7, 6.66, v4);
		
		assertEquals(new Vector(21.0, -42.42), rec2.getPosition());
		assertSame(v3, rec2.getPosition());
		assertEquals(new Vector(-127.128, 0.0), rec2.getMovement());
		assertSame(v4, rec2.getMovement());
		
		assertEquals(10.7, rec2.getWidth());
		assertEquals(6.66, rec2.getHeight());
		
	}
	
	public void testRectangleArea() {
		
		double a = 10.0;
		double b = 5.0;
		Rectangle rec1 = new Rectangle(new Vector(0,0), a, b, new Vector(0,0));
		
		assertEquals(a*b, rec1.area());
		
		a = 22.4;
		b = 3.14;
		Rectangle rec2 = new Rectangle(new Vector(1,7), a, b, new Vector(3,2));
		
		assertEquals(a*b, rec2.area());
		
		a = 2.0;
		b = 1.3;
		Rectangle rec3 = new Rectangle(new Vector(-2,-4), a, b, new Vector(-5,-3));
		
		assertEquals(a*b, rec3.area());
		
	}
}
