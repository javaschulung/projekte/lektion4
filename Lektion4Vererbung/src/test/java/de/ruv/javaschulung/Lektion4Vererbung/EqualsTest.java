package de.ruv.javaschulung.Lektion4Vererbung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class EqualsTest extends TestCase {

	public EqualsTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(EqualsTest.class);
	}

	public void testVectorEquals() {
		
		Vector v1 = new Vector(1.0, 1.0);
		Vector v2 = new Vector(1.0, -1.0);
		Vector v3 = new Vector(-1.0, 1.0);
		Vector v4 = new Vector(-1.0, 1.0);
		Vector v5 = new Vector(1.0, 1.0);
		Vector v6 = new Vector(1.0, -1.0);
		
		assertFalse(v1.equals(v2));
		assertFalse(v1.equals(v3));
		assertFalse(v1.equals(v4));
		
		assertTrue(v1.equals(v1));
		assertTrue(v1.equals(v5));
		assertTrue(v5.equals(v1));
		
		assertFalse(v2.equals(v1));
		assertFalse(v3.equals(v1));
		assertFalse(v4.equals(v1));
		
		assertTrue(v2.equals(v2));
		assertTrue(v2.equals(v6));
		assertTrue(v6.equals(v2));
	}
	
	public void testRectangleEquals() {
		
		Rectangle rec1 = new Rectangle(new Vector(1.0, 1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		Rectangle rec2 = new Rectangle(new Vector(2.0, -1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		Rectangle rec3 = new Rectangle(new Vector(1.0, 1.0), 5.0, 20.5, new Vector(-1.0, -1.0));
		Rectangle rec4 = new Rectangle(new Vector(1.0, 1.0), 10.0, 23.7, new Vector(-1.0, -1.0));
		Rectangle rec5 = new Rectangle(new Vector(1.0, 1.0), 10.0, 23.7, new Vector(1.0, 7.3));
		Rectangle rec6 = new Rectangle(new Vector(1.0, 1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		Rectangle rec7 = new Rectangle(new Vector(2.0, -1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		
		assertFalse(rec1.equals(rec2));
		assertFalse(rec1.equals(rec3));
		assertFalse(rec1.equals(rec4));
		assertFalse(rec1.equals(rec5));
		
		assertTrue(rec1.equals(rec1));
		assertTrue(rec1.equals(rec6));
		assertTrue(rec6.equals(rec1));
		
		assertFalse(rec2.equals(rec1));
		assertFalse(rec3.equals(rec1));
		assertFalse(rec4.equals(rec1));
		assertFalse(rec5.equals(rec1));
		
		assertTrue(rec2.equals(rec2));
		assertTrue(rec2.equals(rec7));
		assertTrue(rec7.equals(rec2));
	}
	
	public void testOvalEquals() {
		
		Oval ova1 = new Oval(new Vector(1.0, 1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		Oval ova2 = new Oval(new Vector(2.0, -1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		Oval ova3 = new Oval(new Vector(1.0, 1.0), 5.0, 20.5, new Vector(-1.0, -1.0));
		Oval ova4 = new Oval(new Vector(1.0, 1.0), 10.0, 23.7, new Vector(-1.0, -1.0));
		Oval ova5 = new Oval(new Vector(1.0, 1.0), 10.0, 23.7, new Vector(1.0, 7.3));
		Oval ova6 = new Oval(new Vector(1.0, 1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		Oval ova7 = new Oval(new Vector(2.0, -1.0), 10.0, 20.5, new Vector(-1.0, -1.0));
		
		assertFalse(ova1.equals(ova2));
		assertFalse(ova1.equals(ova3));
		assertFalse(ova1.equals(ova4));
		assertFalse(ova1.equals(ova5));
		
		assertTrue(ova1.equals(ova1));
		assertTrue(ova1.equals(ova6));
		assertTrue(ova6.equals(ova1));
		
		assertFalse(ova2.equals(ova1));
		assertFalse(ova3.equals(ova1));
		assertFalse(ova4.equals(ova1));
		assertFalse(ova5.equals(ova1));
		
		assertTrue(ova2.equals(ova2));
		assertTrue(ova2.equals(ova7));
		assertTrue(ova7.equals(ova2));
		
	}
	
	public void testCircleEquals() {
		
		Circle cir1 = new Circle(new Vector(1.0, 1.0), 10.0, new Vector(-1.0, -1.0));
		Circle cir2 = new Circle(new Vector(2.0, -1.0), 10.0, new Vector(-1.0, -1.0));
		Circle cir3 = new Circle(new Vector(1.0, 1.0), 5.0, new Vector(-1.0, -1.0));
		Circle cir5 = new Circle(new Vector(1.0, 1.0), 10.0, new Vector(1.0, 7.3));
		Circle cir6 = new Circle(new Vector(1.0, 1.0), 10.0, new Vector(-1.0, -1.0));
		Circle cir7 = new Circle(new Vector(2.0, -1.0), 10.0, new Vector(-1.0, -1.0));
		
		
		assertFalse(cir1.equals(cir2));
		assertFalse(cir1.equals(cir3));
		assertFalse(cir1.equals(cir5));
		
		assertTrue(cir1.equals(cir1));
		assertTrue(cir1.equals(cir6));
		assertTrue(cir6.equals(cir1));
		
		assertFalse(cir2.equals(cir1));
		assertFalse(cir3.equals(cir1));
		assertFalse(cir5.equals(cir1));
		
		assertTrue(cir2.equals(cir2));
		assertTrue(cir2.equals(cir7));
		assertTrue(cir7.equals(cir2));
		
		Oval ova1 = new Oval(new Vector(1.0, 1.0), 10.0, 10.0, new Vector(-1.0, -1.0));
		Oval ova2 = new Oval(new Vector(2.0, -1.0), 10.0, 10.0, new Vector(-1.0, -1.0));
		Oval ova3 = new Oval(new Vector(1.0, 1.0), 5.0, 10.0, new Vector(-1.0, -1.0));
		Oval ova4 = new Oval(new Vector(1.0, 1.0), 10.0, 15.0, new Vector(-1.0, -1.0));
		
		assertTrue(cir1.equals(ova1));
		assertTrue(ova1.equals(cir1));
		assertTrue(cir2.equals(ova2));
		assertTrue(cir2.equals(ova2));
		
		assertFalse(cir1.equals(ova3));
		assertFalse(cir1.equals(ova4));
		assertFalse(cir2.equals(ova3));
		assertFalse(cir2.equals(ova4));
		
		assertFalse(ova3.equals(cir1));
		assertFalse(ova4.equals(cir1));
		assertFalse(ova3.equals(cir2));
		assertFalse(ova4.equals(cir2));
	}
}
