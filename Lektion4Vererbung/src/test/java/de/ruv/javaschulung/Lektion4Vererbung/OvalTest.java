package de.ruv.javaschulung.Lektion4Vererbung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class OvalTest extends TestCase {

	public OvalTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(OvalTest.class);
	}

	public void testOvalKonstruktion() {
		
		Vector v1 = new Vector(-1.0, 1.0);
		Vector v2 = new Vector(2.0, -1.5);
		
		Oval ova1 = new Oval(v1, 3.14, 2.8, v2);
		
		assertEquals(new Vector(-1.0, 1.0), ova1.getPosition());
		assertSame(v1, ova1.getPosition());
		assertEquals(new Vector(2.0, -1.5), ova1.getMovement());
		assertSame(v2, ova1.getMovement());
		
		assertEquals(3.14, ova1.getWidth());
		assertEquals(2.8, ova1.getHeight());
		
		Vector v3 = new Vector(21.0, -42.42);
		Vector v4 = new Vector(-127.128, 0.0);
		
		Oval ova2 = new Oval(v3, 10.7, 6.66, v4);
		
		assertEquals(new Vector(21.0, -42.42), ova2.getPosition());
		assertSame(v3, ova2.getPosition());
		assertEquals(new Vector(-127.128, 0.0), ova2.getMovement());
		assertSame(v4, ova2.getMovement());
		
		assertEquals(10.7, ova2.getWidth());
		assertEquals(6.66, ova2.getHeight());
		
	}
	
	public void testOvalArea() {
		
		double a = 10.0;
		double b = 5.0;
		Oval ova1 = new Oval(new Vector(0,0), a, b, new Vector(0,0));
		
		assertEquals(a*b*Math.PI, ova1.area());
		
		a = 22.4;
		b = 3.14;
		Oval ova2 = new Oval(new Vector(1,7), a, b, new Vector(3,2));
		
		assertEquals(a*b*Math.PI, ova2.area());
		
		a = 2.0;
		b = 1.3;
		Oval ova3 = new Oval(new Vector(-2,-4), a, b, new Vector(-5,-3));
		
		assertEquals(a*b*Math.PI, ova3.area());
		
	}
}
