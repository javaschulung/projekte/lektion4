package de.ruv.javaschulung.Lektion4Vererbung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ToStringTest extends TestCase {

	public ToStringTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(ToStringTest.class);
	}

	public void testDatumToString() {
		
		Datum d1 = new Datum(24, 12, 1913);
		assertEquals("24.12.1913", d1.toString());
		
		Datum d2 = new Datum(1, 1, 1111);
		assertEquals("01.01.1111", d2.toString());
		
		Datum d3 = new Datum(2, 4, 2014);
		assertEquals("02.04.2014", d3.toString());
		
		
	}
}
