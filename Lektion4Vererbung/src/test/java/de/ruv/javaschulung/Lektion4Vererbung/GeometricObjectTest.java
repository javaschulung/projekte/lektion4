package de.ruv.javaschulung.Lektion4Vererbung;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class GeometricObjectTest extends TestCase {

	public GeometricObjectTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(GeometricObjectTest.class);
	}

	public void testGeometricObjectMethods() {
		
		Class<GeometricObject> geoClass = GeometricObject.class;
		
		assertTrue("Klasse ist nicht abstrakt!", Modifier.isAbstract(geoClass.getModifiers()));
		
		try {
			Method meth = geoClass.getMethod("getPosition", new Class[0]);
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", Vector.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode getPosition() fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("getMovement", new Class[0]);
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", Vector.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode getMovement() fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("getWidth", new Class[0]);
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", double.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode getWidth() fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("getHeight", new Class[0]);
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", double.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode getHeight() fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("move", new Class[]{Vector.class} );
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode move(Vector v) fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("move", new Class[0]);
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode move() fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("moveTo", new Class[]{Vector.class} );
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", void.class, meth.getReturnType());
			
		} catch (Exception e) {
			fail("Methode moveTo(Vector v) fehlt oder wurde falsch implementiert!");
		}
		
		try {
			Method meth = geoClass.getMethod("area", new Class[0]);
			assertNotNull(meth);
			
			assertEquals("Rückgabetyp", double.class, meth.getReturnType());
			
			assertTrue("Methode ist nicht abstrakt", Modifier.isAbstract(meth.getModifiers()));
			
		} catch (Exception e) {
			fail("Methode getHeight() fehlt oder wurde falsch implementiert!");
		}
	}
	
	public void testGeometricObjecMoving() {
		Vector v1 = new Vector(1.0, 1.0);
		Vector v2 = new Vector(-1.0, -2.0);
		
		GeometricObject geo = new GeometricObject(v1, 10.0, 5.0, v2) {			
			@Override
			public double area() {
				return 0;
			}
		};
		
		assertEquals(new Vector(1.0, 1.0), geo.getPosition());
		assertEquals(new Vector(-1.0, -2.0), geo.getMovement());
		assertSame(v2, geo.getMovement());
		assertEquals(10.0, geo.getWidth());
		assertEquals(5.0, geo.getHeight());
		
		geo.move(new Vector(42.0, 42.0));
		assertEquals(new Vector(43.0, 43.0), geo.getPosition());
		assertNotSame(v1, geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		geo.move(new Vector(-21.0, -21.0));
		assertEquals(new Vector(22.0, 22.0), geo.getPosition());
		assertNotSame(v1, geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		geo.move(new Vector(-30.0, -10.0));
		assertEquals(new Vector(-8.0, 12.0), geo.getPosition());
		assertNotSame(v1, geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		geo.move();
		assertEquals(new Vector(-9.0, 10.0), geo.getPosition());
		assertNotSame(v1, geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		geo.move();
		assertEquals(new Vector(-10.0, 8.0), geo.getPosition());
		assertNotSame(v1, geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		geo.move();
		assertEquals(new Vector(-11.0, 6.0), geo.getPosition());
		assertNotSame(v1, geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		Vector v3 = new Vector(12.0, -33.4);
		geo.moveTo(v3);
		assertEquals(new Vector(12.0, -33.4), geo.getPosition());
		assertSame(v2, geo.getMovement());
		
		Vector v4 = new Vector(-12.0, 33.4);
		geo.moveTo(v4);
		assertEquals(new Vector(-12.0, 33.4), geo.getPosition());
		assertSame(v2, geo.getMovement());
	}
}
