package de.ruv.javaschulung.Lektion4Vererbung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class CircleTest extends TestCase {

	public CircleTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(CircleTest.class);
	}

	public void testCircleKonstruktion() {
		
		Vector v1 = new Vector(-1.0, 1.0);
		Vector v2 = new Vector(2.0, -1.5);
		
		Circle ova1 = new Circle(v1, 3.14, v2);
		
		assertEquals(new Vector(-1.0, 1.0), ova1.getPosition());
		assertSame(v1, ova1.getPosition());
		assertEquals(new Vector(2.0, -1.5), ova1.getMovement());
		assertSame(v2, ova1.getMovement());
		
		assertEquals(3.14, ova1.getWidth());
		assertEquals(3.14, ova1.getHeight());
		
		Vector v3 = new Vector(21.0, -42.42);
		Vector v4 = new Vector(-127.128, 0.0);
		
		Circle ova2 = new Circle(v3, 10.7, v4);
		
		assertEquals(new Vector(21.0, -42.42), ova2.getPosition());
		assertSame(v3, ova2.getPosition());
		assertEquals(new Vector(-127.128, 0.0), ova2.getMovement());
		assertSame(v4, ova2.getMovement());
		
		assertEquals(10.7, ova2.getWidth());
		assertEquals(10.7, ova2.getHeight());
		
	}
	
	public void testCircleArea() {
		
		double a = 10.0;
		Circle ova1 = new Circle(new Vector(0,0), a, new Vector(0,0));
		
		assertEquals(a*a*Math.PI, ova1.area());
		
		a = 22.4;
		Circle ova2 = new Circle(new Vector(1,7), a, new Vector(3,2));
		
		assertEquals(a*a*Math.PI, ova2.area());
		
		a = 2.0;
		Circle ova3 = new Circle(new Vector(-2,-4), a, new Vector(-5,-3));
		
		assertEquals(a*a*Math.PI, ova3.area());
		
	}
}
