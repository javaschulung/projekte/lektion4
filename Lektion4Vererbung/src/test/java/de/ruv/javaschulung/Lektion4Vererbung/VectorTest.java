package de.ruv.javaschulung.Lektion4Vererbung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class VectorTest extends TestCase {

	public VectorTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(VectorTest.class);
	}

	public void testVectorKonstruktion() {
		
		Vector v1 = new Vector(1.0, 1.0);
		assertEquals(1.0, v1.getX());
		assertEquals(1.0, v1.getY());
		
		Vector v2 = new Vector(-2.9, 3.14);
		assertEquals(-2.9, v2.getX());
		assertEquals(3.14, v2.getY());
		
		Vector v3 = new Vector(-3.14, 2.9);
		assertEquals(-3.14, v3.getX());
		assertEquals(2.9, v3.getY());
		
		Vector v4 = new Vector(-21.0, -42.0);
		assertEquals(-21.0, v4.getX());
		assertEquals(-42.0, v4.getY());
	}
}
